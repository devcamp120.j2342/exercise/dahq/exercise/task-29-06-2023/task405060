package com.devcamp.customerapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerapi.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
